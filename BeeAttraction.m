function varargout = BeeAttraction(varargin)
% BEEATTRACTION MATLAB code for BeeAttraction.fig
%      BEEATTRACTION, by itself, creates a new BEEATTRACTION or raises the existing
%      singleton*.
%
%      H = BEEATTRACTION returns the handle to a new BEEATTRACTION or the handle to
%      the existing singleton*.
%
%      BEEATTRACTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BEEATTRACTION.M with the given input arguments.
%
%      BEEATTRACTION('Property','Value',...) creates a new BEEATTRACTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BeeAttraction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BeeAttraction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BeeAttraction

% Last Modified by GUIDE v2.5 07-Nov-2018 16:19:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BeeAttraction_OpeningFcn, ...
                   'gui_OutputFcn',  @BeeAttraction_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BeeAttraction is made visible.
function BeeAttraction_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BeeAttraction (see VARARGIN)

%defualt parameters
handles.attraction = 0.0; % no attraction
handles.totalTimePoints = 150; % number of time points
handles.output = hObject;
addpath(genpath('beefiles'))

Beeimage = imread('beefiles/Beeimage.jpg');
axes(handles.Beeimage);
imshow(Beeimage)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BeeAttraction wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BeeAttraction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Strengthvalue_Callback(hObject, eventdata, handles)
% hObject    handle to Strengthvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Strengthvalue as text
%        str2double(get(hObject,'String')) returns contents of Strengthvalue as a double
handles.attraction = str2double(get(hObject,'String'))/100;
% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function Strengthvalue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Strengthvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in simlength.
function simlength_Callback(hObject, eventdata, handles)
% hObject    handle to simlength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
% Hints: contents = cellstr(get(hObject,'String')) returns simlength contents as cell array
%        contents{get(hObject,'Value')} returns selected item from simlength
items = get(hObject,'String');
index_selected = get(hObject,'Value');

if index_selected == 1
    handles.totalTimePoints = 150;
    
elseif index_selected == 2
    handles.totalTimePoints = 300;
    
elseif index_selected == 3
    handles.totalTimePoints = 1500;
    
end

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function simlength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to simlength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Runsim.
function Runsim_Callback(hObject, eventdata, handles)
% hObject    handle to Runsim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.Simulation);
simulation_attraction(hObject, eventdata, handles)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over simlength.
function simlength_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to simlength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function Beeimage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Beeimage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Beeimage


