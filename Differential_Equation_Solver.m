%Differential Equation Solver Main GUI
%Accepts user input system of ordinary differential equations and explicit
%equations, uses ODE45 to solve them, and plots results for each
%differential equation on a graph. User can edit the list of equations at
%any time. User can specify the axis labels on the graph. User can save the
%graph figure as a .png file with a user genrated filename to their computer. User can save the equation
%solution data to an excel file with a user generated filename on thier computer.

function varargout = Differential_Equation_Solver(varargin)
% DIFFERENTIAL_EQUATION_SOLVER MATLAB code for Differential_Equation_Solver.fig
%      DIFFERENTIAL_EQUATION_SOLVER, by itself, creates a new DIFFERENTIAL_EQUATION_SOLVER or raises the existing
%      singleton*.
%
%      H = DIFFERENTIAL_EQUATION_SOLVER returns the handle to a new DIFFERENTIAL_EQUATION_SOLVER or the handle to
%      the existing singleton*.
%
%      DIFFERENTIAL_EQUATION_SOLVER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIFFERENTIAL_EQUATION_SOLVER.M with the given input arguments.
%
%      DIFFERENTIAL_EQUATION_SOLVER('Property','Value',...) creates a new DIFFERENTIAL_EQUATION_SOLVER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Differential_Equation_Solver_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Differential_Equation_Solver_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Differential_Equation_Solver

% Last Modified by GUIDE v2.5 06-Dec-2018 22:27:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Differential_Equation_Solver_OpeningFcn, ...
                   'gui_OutputFcn',  @Differential_Equation_Solver_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Differential_Equation_Solver is made visible.
function Differential_Equation_Solver_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Differential_Equation_Solver (see VARARGIN)

% Choose default command line output for Differential_Equation_Solver
handles.output = hObject;

%population the ODE list box

diffeqnum = getappdata(0,'diff_eq_num');
differentiallistitems = [];

for i = 1 : diffeqnum
    
    %get data from other GUI
    diff_rhs = getappdata(0,strcat('diff_rhs',num2str(i)));
    numerator = getappdata(0,strcat('numerator',num2str(i)));
    denominator = getappdata(0,strcat('denominator',num2str(i)));
    initial_rhs = getappdata(0,strcat('initial_rhs',num2str(i)));
    
    %combine to put in list
    DiffEQ(i) = "d"+ numerator +" / d"+ denominator + " = " + diff_rhs + "   ;   y(0) = " + initial_rhs;
    
    differentiallistitems = DiffEQ;
    
end

set(handles.differential_list,'string',differentiallistitems);



%populating the explicit equation list box
expleqnum = getappdata(0,'expl_eq_num');
explicitlistitems = [];

for i = 1 : expleqnum
    
    %get data from other gui
    exlhsrhs = getappdata(0,strcat('explicit_equation',num2str(i)));
    
    %format for list
    expleq(i) = exlhsrhs + " ";
   
    explicitlistitems = expleq;
  
end

set(handles.explicit_list,'string',explicitlistitems);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Differential_Equation_Solver wait for user response (see UIRESUME)
% uiwait(handles.Differential_Equation_Solver);


% --- Outputs from this function are returned to the command line.
function varargout = Differential_Equation_Solver_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in define_system.
function define_system_Callback(hObject, eventdata, handles)
% hObject    handle to define_system (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%initiate next gui
Number_Equations();
close(handles.Differential_Equation_Solver);

% --- Executes on button press in calculate_results.
function calculate_results_Callback(hObject, eventdata, handles)
% hObject    handle to calculate_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Calling the ODE solver function
% The span of the volume goes from 0 to 1 and is calculated in intervals of
% 0.01. The initial conditions where the algorithm will start its
% calculation are also given before calling the ODE45 solver which takes
% the system of ODEs function and solves it.

%get data from other gui
lowerlimit = getappdata(0,'lowerlimit');
upperlimit = getappdata(0,'upperlimit');

diffeqnum = getappdata(0,'diff_eq_num');
expleqnum = getappdata(0,'expl_eq_num');

for i = 1 : diffeqnum
    
    denominatortemp = getappdata(0,strcat('denominator',num2str(i)));
    numeratortemp = getappdata(0,strcat('numerator',num2str(i)));
    diffrhstemp = getappdata(0,strcat('diff_rhs',num2str(i)));
    initialrhstemp = getappdata(0,strcat('initial_rhs',num2str(i))); 
    
    denominator{i} = denominatortemp;
    denominators = denominator;
    
    numerator{i} = numeratortemp;
    numerators = numerator;
    
    diffrhs{i} = diffrhstemp;
    diffrhss = diffrhs;
    
    initialrhs{i} = initialrhstemp;
    initialrhss = initialrhs;
    
    
end

for i = 1 : expleqnum
    
    explhsrhstemp = getappdata(0,strcat('explicit_equation',num2str(i)));

    explhsrhs{i} = explhsrhstemp;
    expliciteqs = explhsrhs;
        
end

%set ODE solver conditions and initial data
interval = (upperlimit-lowerlimit)/100;
span = (lowerlimit:interval:upperlimit);
initialcondition = str2num(char(initialrhss'));
setappdata(0,'initialrhss',initialrhss);

%call ODE solver to solve input equations
[t,y] = ode45(@ODESolve, span, initialcondition, diffrhs, denominators, numerators, diffrhss, expliciteqs);

%save results
setappdata(0,'y',y);
setappdata(0,'t',t);

msgbox('Results calculated')

% Definning the system of ODEs
% A function defines the equations in the system of ODEs to use in the
% ODE45 function.

%function to create and solve the user entered system of differential equations
function dydt = ODESolve(t, y, denominators, numerators, diffrhss, expliciteqs)

%get data from other gui
initialrhss = getappdata(0,'initialrhss');

%get 1st equation's demonimator. should be same for all differential equations in this
%system
str0=cell2mat(denominators(1));
eval(strcat(str0,'=t;'));

%find each numerator and set equal to initial condition
for i = 1 : length(numerators)
    str1 = cell2mat(numerators(i));
    eval(strcat(str1,'=y(i);'));
end

%enters and evaluates explicit equations
for i = 1 : length(expliciteqs)
    str2 = cell2mat(expliciteqs(i));
    eval(strcat(str2,';'));
end

%enters and evaluates the differential equations
for i = 1 : length(diffrhss)
    str3 = cell2mat(diffrhss(i));
    dydt(i) = eval(strcat(str3,';'));
end

%outputs results for each differential equation
dydt = dydt';


% --- Executes on button press in plot_results.
function plot_results_Callback(hObject, eventdata, handles)
% hObject    handle to plot_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%opens new gui and gets user entered axis labels
Enter_Axis_Labels()
uiwait;

xaxistext = getappdata(0,'xaxistext');
yaxistext = getappdata(0,'yaxistext');

%get data from other gui
diffeqnum = getappdata(0,'diff_eq_num');

for i = 1 : diffeqnum
    
    numeratortemp = getappdata(0,strcat('numerator',num2str(i))); 
    numerator{i} = numeratortemp;
    numerators = numerator;
   
end

y = getappdata(0,'y');
t = getappdata(0,'t');

%plots ODEsolver results and the user input axis labels and creates legend
%with numerators as the line label
plot(t,y)
xlabel(xaxistext)
ylabel(yaxistext)

legend(numerator,'Location','Best')



% --- Executes on button press in save_plot.
function save_plot_Callback(hObject, eventdata, handles)
% hObject    handle to save_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%opens new gui and gets user input data for the filename for the saved .png
%of the figure
Enter_filename();
uiwait;
figurename = getappdata(0,'figurename');

%isolates figure picture and exports it as a .png file
ax = handles.axes1;
figure_handle = isolate_axes(ax);
export_fig(figure_handle,strcat(figurename), '-png')


% --- Executes on button press in export_results.
function export_results_Callback(hObject, eventdata, handles)
% hObject    handle to export_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%opens new gui and gets user input for the excel file name
Enter_Excel_Filename();
uiwait;

%get data from ODE solver and labels for the data to be saved in excel
excelname = getappdata(0,'excelname');
exceldata = [getappdata(0,'t'),getappdata(0,'y')];
diffeqnum = getappdata(0,'diff_eq_num');

for i = 1 : diffeqnum
    
    numeratortemp{i} = getappdata(0,strcat('numerator',num2str(i))) ;
    numerator = convertCharsToStrings(numeratortemp);
    
    denominatortemp{i} = getappdata(0,strcat('denominator',num2str(i)));
    denominator = convertCharsToStrings(denominatortemp);
   
end

%format columns and make table
columnnames = [denominator(1),numerator];
exceldata = [columnnames;exceldata];
exceltable = table(exceldata);

%save table of data in excel file named by the user
writetable(exceltable,strcat(excelname,'.xlsx'),'WriteVariableNames',false);

% --- Executes on selection change in differential_list.
function differential_list_Callback(hObject, eventdata, handles)
% hObject    handle to differential_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns differential_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from differential_list


% --- Executes during object creation, after setting all properties.
function differential_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to differential_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in explicit_list.
function explicit_list_Callback(hObject, eventdata, handles)
% hObject    handle to explicit_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns explicit_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from explicit_list



% --- Executes during object creation, after setting all properties.
function explicit_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to explicit_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in edit_differential_equation.
function edit_differential_equation_Callback(hObject, eventdata, handles)
% hObject    handle to edit_differential_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%identify which equation is selected in the listbox
diffcount = get(handles.differential_list,'value');
setappdata(0,'diffcount',diffcount);

%open gui to allow user to input new equation to replace old equation
Enter_Differential_Equation();
uiwait;
Differential_Equation_Solver();



% --- Executes on button press in edit_explicit_equation.
function edit_explicit_equation_Callback(hObject, eventdata, handles)
% hObject    handle to edit_explicit_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)     

%identify which equation is selected in the listbox
explcount = get(handles.explicit_list,'value');
setappdata(0,'explcount',explcount);

%open gui to allow user to input new equation to replace old equation
Enter_Explicit_Equation();
uiwait;
Differential_Equation_Solver();
