function varargout = Enter_Differential_Equation(varargin)
% ENTER_DIFFERENTIAL_EQUATION MATLAB code for Enter_Differential_Equation.fig
%      ENTER_DIFFERENTIAL_EQUATION, by itself, creates a new ENTER_DIFFERENTIAL_EQUATION or raises the existing
%      singleton*. 
%
%      H = ENTER_DIFFERENTIAL_EQUATION returns the handle to a new ENTER_DIFFERENTIAL_EQUATION or the handle to
%      the existing singleton*.
%
%      ENTER_DIFFERENTIAL_EQUATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENTER_DIFFERENTIAL_EQUATION.M with the given input arguments.
%
%      ENTER_DIFFERENTIAL_EQUATION('Property','Value',...) creates a new ENTER_DIFFERENTIAL_EQUATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Enter_Differential_Equation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Enter_Differential_Equation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Enter_Differential_Equation

% Last Modified by GUIDE v2.5 05-Dec-2018 23:35:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Enter_Differential_Equation_OpeningFcn, ...
                   'gui_OutputFcn',  @Enter_Differential_Equation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Enter_Differential_Equation is made visible.
function Enter_Differential_Equation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Enter_Differential_Equation (see VARARGIN)

% Choose default command line output for Enter_Differential_Equation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes Enter_Differential_Equation wait for user response (see UIRESUME)
% uiwait(handles.Enter_Differential_Equation);


% --- Outputs from this function are returned to the command line.
function varargout = Enter_Differential_Equation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function numerator_Callback(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numerator as text
%        str2double(get(hObject,'String')) returns contents of numerator as a double

%collect user input
handles.numerator = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function numerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function denominator_Callback(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of denominator as text
%        str2double(get(hObject,'String')) returns contents of denominator as a double

%collect user input
handles.denominator = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function denominator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function diff_rhs_Callback(hObject, eventdata, handles)
% hObject    handle to diff_rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of diff_rhs as text
%        str2double(get(hObject,'String')) returns contents of diff_rhs as a double

%collect user input
handles.diff_rhs = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function diff_rhs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to diff_rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function initial_rhs_Callback(hObject, eventdata, handles)
% hObject    handle to initial_rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initial_rhs as text
%        str2double(get(hObject,'String')) returns contents of initial_rhs as a double

%collect user input
handles.initial_rhs = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function initial_rhs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initial_rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in done_button_diff.
function done_button_diff_Callback(hObject, eventdata, handles)
% hObject    handle to done_button_diff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%saves collected input in an array which numbers the equations based on what order they were entered 
diffcount = getappdata(0,'diffcount');

setappdata(0,strcat('diff_rhs',num2str(diffcount)),handles.diff_rhs);
setappdata(0,strcat('numerator',num2str(diffcount)),handles.numerator);
setappdata(0,strcat('denominator',num2str(diffcount)),handles.denominator);
setappdata(0,strcat('initial_rhs',num2str(diffcount)),handles.initial_rhs);

close(Enter_Differential_Equation);
