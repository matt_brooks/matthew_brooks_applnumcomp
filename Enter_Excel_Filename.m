function varargout = Enter_Excel_Filename(varargin)
% ENTER_EXCEL_FILENAME MATLAB code for Enter_Excel_Filename.fig
%      ENTER_EXCEL_FILENAME, by itself, creates a new ENTER_EXCEL_FILENAME or raises the existing
%      singleton*. 
%
%      H = ENTER_EXCEL_FILENAME returns the handle to a new ENTER_EXCEL_FILENAME or the handle to
%      the existing singleton*.
%
%      ENTER_EXCEL_FILENAME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENTER_EXCEL_FILENAME.M with the given input arguments.
%
%      ENTER_EXCEL_FILENAME('Property','Value',...) creates a new ENTER_EXCEL_FILENAME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Enter_Excel_Filename_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Enter_Excel_Filename_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Enter_Excel_Filename

% Last Modified by GUIDE v2.5 06-Dec-2018 22:27:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Enter_Excel_Filename_OpeningFcn, ...
                   'gui_OutputFcn',  @Enter_Excel_Filename_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Enter_Excel_Filename is made visible.
function Enter_Excel_Filename_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Enter_Excel_Filename (see VARARGIN)

% Choose default command line output for Enter_Excel_Filename
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Enter_Excel_Filename wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Enter_Excel_Filename_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function excelfilename_Callback(hObject, eventdata, handles)
% hObject    handle to excelfilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of excelfilename as text
%        str2double(get(hObject,'String')) returns contents of excelfilename as a double

%collect user input
handles.excelname = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function excelfilename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to excelfilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in enterexcelname.
function enterexcelname_Callback(hObject, eventdata, handles)
% hObject    handle to enterexcelname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%save user input
setappdata(0,'excelname',handles.excelname)

close(Enter_Excel_Filename)
