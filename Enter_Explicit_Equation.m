function varargout = Enter_Explicit_Equation(varargin)
% ENTER_EXPLICIT_EQUATION MATLAB code for Enter_Explicit_Equation.fig
%      ENTER_EXPLICIT_EQUATION, by itself, creates a new ENTER_EXPLICIT_EQUATION or raises the existing
%      singleton*. 
%
%      H = ENTER_EXPLICIT_EQUATION returns the handle to a new ENTER_EXPLICIT_EQUATION or the handle to
%      the existing singleton*.
%
%      ENTER_EXPLICIT_EQUATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENTER_EXPLICIT_EQUATION.M with the given input arguments.
%
%      ENTER_EXPLICIT_EQUATION('Property','Value',...) creates a new ENTER_EXPLICIT_EQUATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Enter_Explicit_Equation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Enter_Explicit_Equation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Enter_Explicit_Equation

% Last Modified by GUIDE v2.5 26-Nov-2018 15:56:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Enter_Explicit_Equation_OpeningFcn, ...
                   'gui_OutputFcn',  @Enter_Explicit_Equation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Enter_Explicit_Equation is made visible.
function Enter_Explicit_Equation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Enter_Explicit_Equation (see VARARGIN)

% Choose default command line output for Enter_Explicit_Equation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Enter_Explicit_Equation wait for user response (see UIRESUME)
% uiwait(handles.Enter_Explicit_Equation);


% --- Outputs from this function are returned to the command line.
function varargout = Enter_Explicit_Equation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function explicit_equation_Callback(hObject, eventdata, handles)
% hObject    handle to explicit_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of explicit_equation as text
%        str2double(get(hObject,'String')) returns contents of explicit_equation as a double

%collect user input
handles.explicit_equation = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function explicit_equation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to explicit_equation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in done_button_explicit.
function done_button_explicit_Callback(hObject, eventdata, handles)
% hObject    handle to done_button_explicit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%saves collected input in an array which numbers the equations based on what order they were entered
explcount = getappdata(0,'explcount');

setappdata(0,strcat('explicit_equation',num2str(explcount)),handles.explicit_equation);

close(handles.Enter_Explicit_Equation)
    
