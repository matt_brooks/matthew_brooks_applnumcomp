function varargout = Enter_filename(varargin)
% ENTER_FILENAME MATLAB code for Enter_filename.fig
%      ENTER_FILENAME, by itself, creates a new ENTER_FILENAME or raises the existing
%      singleton*. 
%
%      H = ENTER_FILENAME returns the handle to a new ENTER_FILENAME or the handle to
%      the existing singleton*.
%
%      ENTER_FILENAME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENTER_FILENAME.M with the given input arguments.
%
%      ENTER_FILENAME('Property','Value',...) creates a new ENTER_FILENAME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Enter_filename_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Enter_filename_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Enter_filename

% Last Modified by GUIDE v2.5 05-Dec-2018 23:44:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Enter_filename_OpeningFcn, ...
                   'gui_OutputFcn',  @Enter_filename_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Enter_filename is made visible.
function Enter_filename_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Enter_filename (see VARARGIN)

% Choose default command line output for Enter_filename
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Enter_filename wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Enter_filename_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function figurename_Callback(hObject, eventdata, handles)
% hObject    handle to figurename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of figurename as text
%        str2double(get(hObject,'String')) returns contents of figurename as a double

%collect user input
handles.figurename = get(hObject,'String');

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function figurename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figurename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in enterfigurenamebutton.
function enterfigurenamebutton_Callback(hObject, eventdata, handles)
% hObject    handle to enterfigurenamebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%save user input
setappdata(0,'figurename',handles.figurename)

close(Enter_filename)
