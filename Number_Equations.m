function varargout = Number_Equations(varargin)
% NUMBER_EQUATIONS MATLAB code for Number_Equations.fig
%      NUMBER_EQUATIONS, by itself, creates a new NUMBER_EQUATIONS or raises the existing
%      singleton*. 
%
%      H = NUMBER_EQUATIONS returns the handle to a new NUMBER_EQUATIONS or the handle to
%      the existing singleton*.
%
%      NUMBER_EQUATIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NUMBER_EQUATIONS.M with the given input arguments.
%
%      NUMBER_EQUATIONS('Property','Value',...) creates a new NUMBER_EQUATIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Number_Equations_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Number_Equations_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Number_Equations

% Last Modified by GUIDE v2.5 27-Nov-2018 14:11:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Number_Equations_OpeningFcn, ...
                   'gui_OutputFcn',  @Number_Equations_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Number_Equations is made visible.
function Number_Equations_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Number_Equations (see VARARGIN)

% Choose default command line output for Number_Equations
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Number_Equations wait for user response (see UIRESUME)
% uiwait(handles.Number_Equations);


% --- Outputs from this function are returned to the command line.
function varargout = Number_Equations_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function diff_eq_num_Callback(hObject, eventdata, handles)
% hObject    handle to diff_eq_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of diff_eq_num as text
%        str2double(get(hObject,'String')) returns contents of diff_eq_num as a double

%save user entered value
handles.diffeqnum = str2double(get(hObject,'String'));
setappdata(0,'diff_eq_num',handles.diffeqnum);

%check to make sure user entered a value and issue warning if not
if handles.diffeqnum == 0
    msgbox('Unexpected Input','Warning','warn')
    uiwait
    Differential_Equation_Solver()
    close(Number_Equations);
end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function diff_eq_num_CreateFcn(hObject, eventdata, handles)
% hObject    handle to diff_eq_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function expl_eq_num_Callback(hObject, eventdata, handles)
% hObject    handle to expl_eq_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of expl_eq_num as text
%        str2double(get(hObject,'String')) returns contents of expl_eq_num as a double

%save user entered values
handles.expleqnum = str2double(get(hObject,'String'));
setappdata(0,'expl_eq_num',handles.expleqnum);

%check to see if user entered a value and issue warning if not
if handles.expleqnum == 0
    msgbox('Unexpected Input','Warning','warn')
    uiwait
    Differential_Equation_Solver()
    close(Number_Equations);
end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function expl_eq_num_CreateFcn(hObject, eventdata, handles)
% hObject    handle to expl_eq_num (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in done_button_number.
function done_button_number_Callback(hObject, eventdata, handles)
% hObject    handle to done_button_number (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%keeps track of how many equations have been entered and opens new gui
%window until the number of equations the user specified have been created.
for diffcount = 1 : handles.diffeqnum 
   
    setappdata(0,'diffcount',diffcount);
    Enter_Differential_Equation();
    uiwait
    
end

for explcount = 1 : handles.expleqnum
   
    setappdata(0,'explcount',explcount);
    Enter_Explicit_Equation();
    uiwait
    
end

%close gui after loops are done and equations are saved
Differential_Equation_Solver()
close(handles.Number_Equations);



function lower_limit_Callback(hObject, eventdata, handles)
% hObject    handle to lower_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lower_limit as text
%        str2double(get(hObject,'String')) returns contents of lower_limit as a double

%saves user input for the lower limit of integration the user wants
%to calculate/graph
handles.lowerlimit = str2double(get(hObject,'String'));
setappdata(0,'lowerlimit',handles.lowerlimit);

%checks to see if user entered a value and issues warning if not
if isempty(handles.lowerlimit) == 1
    msgbox('Unexpected Input','Warning','warn')
    uiwait
    Differential_Equation_Solver()
    close(Number_Equations);
end

% --- Executes during object creation, after setting all properties.
function lower_limit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lower_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function upper_limit_Callback(hObject, eventdata, handles)
% hObject    handle to upper_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of upper_limit as text
%        str2double(get(hObject,'String')) returns contents of upper_limit as a double

%saves user input for the lower limit of integration the user wants
%to calculate/graph
handles.upperlimit = str2double(get(hObject,'String'));
setappdata(0,'upperlimit',handles.upperlimit);

%checks to see if user entered a value and issues warning if not
if isempty(handles.upperlimit) == 1
    msgbox('Unexpected Input','Warning','warn')
    uiwait
    Differential_Equation_Solver()
    close(Number_Equations);
end

% --- Executes during object creation, after setting all properties.
function upper_limit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to upper_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
