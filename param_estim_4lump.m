%% param_estim_4lump
%  Author: Matthew Brooks
%  Date: 10/24/2018 
%
% This program uses initial data and parameter estimation to solve a system 
% differential equations and plot a best fit curve for the conversion of 
% VGO into gasoline, gas, and coke. These substances are split into four 
% lumps: VGO, gasoline, Gas, and Coke. The yield of each of these lumps is
% plotted agianst both the time of the reaction in hours and the percent
% conversion. The parameters are estimated using a given table of yields at
% different times.
% 
% $$\frac{dy_1}{dt} = -(k_{12}+k_{13}+k_{14})*y_1^2$$
%
% $$ \frac{dy_2}{dt} = k_{12}*y_1^2-k_{23}*y_2-k_{24}*y_2$$
%
% $$ \frac{dy_3}{dt} = k_{13}*y_1^2+k_{23}*y_2$$
%
% $$ \frac{dy_4}{dt} = k_{14}*y_1^2+k_{24}*y_2$$
% 
% $$k_i =$ Rate constant of the reaction
% 
% $$y_1 =$ Yield of VGO
% 
% $$y_2 =$ Yield of Gasoline
% 
% $$y_3 =$ Yield of Gas
% 
% $$y_4 =$ Yield of Coke


%% Data used to estimate parameters

% Time
xdata = [ 1/60, 1/30, 1/20, 1/10];

% Yield of y_1; y_2; y_3; y_4 at given times of xdata
ydata = [ .5074, .3796, .2882, .1762; .3767, .4385, .4865, .5416; .0885, .136, .1681, .2108; .0274, .0459, .0572, .0714];

%% Guesses for parameters 
k(1) = 1;
k(2) = 1;
k(3) = 1;
k(4) = 1;
k(5) = 1;
params_guess = k;

%% Initial conditions for ODEs 
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;
y0(4) = 0;

%% Estimate parameters
[params,resnorm,] = lsqcurvefit(@(params,xdata) ...
    ODEmodel3lump(params,xdata,y0),params_guess,xdata,ydata);

%% Plot Results
%Print parameter and curve fit results

%output parameter results
k_12 = params(1)
k_13 = params(2)
k_14 = params(3)
k_23 = params(4)
k_24 = params(5)
resnorm = resnorm

% plot yield vs time graph
figure
subplot (1,2,1)
hold on
plot(xdata,ydata(1,:),'ko')
xlabel('Hours')
ylabel('%')
plot(xdata,ydata(2,:),'ro')
plot(xdata,ydata(3,:),'bo')
plot(xdata,ydata(4,:),'go')

times = linspace(0,.5);
y_calc = ODEmodel3lump(params,times,y0);
plot(times,y_calc(1,:),'k')
plot(times,y_calc(2,:),'r')
plot(times,y_calc(3,:),'b')
plot(times,y_calc(4,:),'g')
hold off
ylim([0 1])
legend('VGO', 'Gasoline', 'Gas', 'Coke')

%plot yield vs conversion graph
subplot (1,2,2)
hold on
plot(1-ydata(1,:),ydata(1,:),'ko')
xlabel('% Conversion')
ylabel('%')
plot(1-ydata(1,:),ydata(2,:),'ro')
plot(1-ydata(1,:),ydata(3,:),'bo')
plot(1-ydata(1,:),ydata(4,:),'go')

timesconversion = linspace(0,1);
y_calc = ODEmodel3lump(params,timesconversion,y0);
plot(1-y_calc(1,:),y_calc(1,:),'k')
plot(1-y_calc(1,:),y_calc(2,:),'r')
plot(1-y_calc(1,:),y_calc(3,:),'b')
plot(1-y_calc(1,:),y_calc(4,:),'g')
hold off
ylim([0 1])
legend('VGO', 'Gasoline', 'Gas', 'Coke')

%% Define system of ODEs 
function dydt = ODE3lump(t,y,params)
    k_12 = params(1);
    k_13 = params(2);
    k_14 = params(3);
    k_23 = params(4);
    k_24 = params(5);
    dydt(1) = -(k_12+k_13+k_14)*(y(1)^2);
    dydt(2) = (k_12*(y(1)^2))-(k_23*y(2))-(k_24*y(2));
    dydt(3) = (k_13*(y(1)^2))+(k_23*y(2));
    dydt(4) = (k_14*(y(1)^2))+(k_24*y(2));
    dydt = dydt';
end

%% Solve system of ODEs
% Uses current params values and xdata as the final point in the tspan for the ODE solver
function y_output = ODEmodel3lump(params,xdata,y0)
    for i = 1:length(xdata)
        tspan = [0:0.0001:xdata(i)+.0001];
        [~,y_calc] = ode23s(@(t,y) ODE3lump(t,y,params),tspan,y0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end