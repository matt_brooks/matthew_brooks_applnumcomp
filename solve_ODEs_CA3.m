function solution = solve_ODEs_CA3()
%% Documentation for solve_ODEs_CA3.m
%  Author: Matthew Brooks
%  Date: 10/03/2018 
%
% This function solves a system differential equations that
% plots the molar flow rates of species A, B, and C and the temperature
% along the volume of a non-isothermal plug-flow reactor. The paramters
% used for this model are listed below and can be changed to fit different
% situations. The system of equations for the reactor are:
%
% $$ \frac{dF_A}{dV} = r_A$$
%
% $$ \frac{dF_B}{dV} = r_B$$
%
% $$ \frac{dF_C}{dV} = r_C$$
%
% $$ \frac{dT}{dV} = \frac{U_a(T_a-T)+(k_{1A}C_A)(- \Delta H_{Rx1A})+(k_{2A}C_A^2)(- \Delta H_{Rx2A})}{F_AC_{P_A}+F_BC_{P_B}+F_CC_{P_C}}$$
%
% $$k_{1A} = 10 \mathrm{exp} \Big[ \frac{E_1}{R} \Big( \frac{1}{300}-\frac{1}{T} \Big) \Big]$$
% 
% $$k_{1A} = 0.09 \mathrm{exp} \Big[ \frac{E_2}{R} \Big( \frac{1}{300}-\frac{1}{T} \Big) \Big]$$
%
% $$ r_A = -k_{1A}C_A-k_{2A}C_A^2$$
%
% $$ r_B = k_{1A}C_A$$
%
% $$ r_C = \frac{1}{2}k_{2A}C_A^2$$
%
% $$ C_A = C_{T0} \Big( \frac{F_A}{F_T} \Big) \Big( \frac{T_0}{T} \Big)$$
%
% $$ C_B = C_{T0} \Big( \frac{F_B}{F_T} \Big) \Big( \frac{T_0}{T} \Big)$$
%
% $$ C_C = C_{T0} \Big( \frac{F_C}{F_T} \Big) \Big( \frac{T_0}{T} \Big)$$
%
% $$ F_T = F_A+F_B+F_C$$
%
% The list of paramters used in this calculation are:
%
% H_r1 - J/(mol of A reacted in reaction 1)
%
% H_r2 - J/(mol of A reacted in reaction 2)
%
% C_pa - J/mol*C
%
% C_pb - J/mol*C
%
% C_pc - J/mol*C
%
% C_t0 - J/mol*C
%
% U_a - J/m^3*s*C
%
% T_a - K
%
% T_0 - K
%
% E1overR - K
%
% E2overR - K
%
% F_a - mol/s
%
% F_b - mol/s
%
% F_c - mol/s
%
% T - k


%% Calling the ODE solver function
% The span of the volume goes from 0 to 1 and is calculated in intervals of
% 0.01. The initial conditions where the algorithm will start its
% calculation are also given before calling the ODE45 solver which takes
% the system of ODEs function and solves it.
vspan = [0:.01:1];
initialcondition = [100;0;0;423];

[V,dt] = ode45(@ODEs_CA3, vspan, initialcondition);

%% Plotting results
% plotting the molar flow rates for species A, B, and C in moles per second 
% vs. volume in dm^3 on one graph.
subplot(1,2,1);
plot(V,dt(:,1),'-')
hold on
plot(V,dt(:,2),'--')
plot(V,dt(:,3),'-.')
xlabel('V (dm^3)')
ylabel('F_i (mol/s)')
legend('F_a','F_b','F_c','Location','Best')
title('Profile of molar flow rates')

% Plotting the temperature in kelvin vs. volume in dm^3 on another graph.

subplot(1,2,2);
plot(V,dt(:,4))
hold off
xlabel('V (dm^3)')
ylabel('T (K)')
legend('T','Location','Best')
title('Temperature profile')

%% Definning the system of ODEs
% A function defines the equations in the system of ODEs to use in the
% ODE45 function.

    function output = ODEs_CA3(V,dt)
        
        % parameters with defualt values
        H_r1 = -20000;
        H_r2 = -60000;
        C_pa = 90;
        C_pb = 90;
        C_pc = 180;
        U_a = 4000;
        T_a = 373;
        E1overR = 4000;
        E2overR = 9000;
        F_a = dt(1);
        F_b = dt(2);
        F_c = dt(3);
        T = dt(4);
        C_t0 = .1;
        T_0 = 423;
        
        % System of differential equations
        k_1a = 10*exp(E1overR*((1/300)-(1/T)));
        k_2a = 0.09*exp(E2overR*((1/300)-(1/T)));
        
        F_t = F_a+F_b+F_c;
        
        C_a = C_t0*(F_a/F_t)*(T_0/T);
        C_b = C_t0*(F_b/F_t)*(T_0/T);
        C_c = C_t0*(F_c/F_t)*(T_0/T);
        
        r_a = -k_1a*C_a-k_2a*C_a^2;
        r_b = k_1a*C_a;
        r_c = (1/2)*k_2a*C_a^2;
        
        dF_a_dV = r_a;
        dF_b_dV = r_b;
        dF_c_dV = r_c;
        dT_dV = ((U_a*(T_a-T))+((k_1a*C_a)*(-H_r1))+((k_2a*C_a^2)*(-H_r2)))/((F_a*C_pa)+(F_b*C_pb)+(F_c*C_pc));
        
        % ouputing differential results
        output = [dF_a_dV;dF_b_dV;dF_c_dV;dT_dV];
        
    end

end

