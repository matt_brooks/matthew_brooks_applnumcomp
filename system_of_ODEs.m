%% system_of_ODEs.m
% system_of_ODEs( $t,  [C_a,C_b],  k_1,  k_2,  k_3,  k_4$ )
%
%   This function takes the inputs
%   for time, concentration a, concentration b, and the 4 rate constants and
%   outputs the solution to these 2 equations equations in a 2x1 matrix.
%   Author: Matthew Brooks
%   Date: 09/10/2018
% 
% $\frac{dC_a}{dt} = -k_1C_a-k_2C_b$
% 
% $\frac{dC_b}{dt} = k_1*C_a-k_3-k_4C_b$

function output = system_of_ODEs(varargin)

%% Set default parameters
%   Create the defualt parameters that the function will use if another
%   input isnt specified by the user.

    t = 0; % time (h)
    c = [6.25, 0]; % concentration (mg * L$^{-1}$)
    k1 = 0.15; % rate constant 1(h$^{-1}$)
    k2 = 0.6; % rate constant 2(h$^{-1}$)
    k3 = 0.1; % rate constant 3(mg L$^{-1}$ h$^{-1}$)
    k4 = 0.2; % rate constant 4(h$^{-1}$)

    
%% Determine how many inputs are submitted to the function
%   How many inputs were submitted is detected and it is determined when to use
%   defualt parameters. This function accepts if a user inputs all the
%   parameters, if they input only the t and C parameters, and if they
%   input no parameters.

% no inputs
if nargin == 0
    %continue with defualt values
    
    
% if t and c are input
elseif nargin == 2
    t = varargin{1};
    c = varargin{2};
    
    
% if t, c, k1, k2, k3, and k4 are input
elseif nargin == 6
    t = varargin{1};
    c = varargin{2};
    k1 = varargin{3};
    k2 = varargin{4};
    k3 = varargin{5};
    k4 = varargin{6};
    
    
% if an invalid number of variables are input
else
    %error message
    
end


%% Calculate the derivative values
%   The values of the two differential equations are calculated with the
%   given parameters.

dcadt = -k1*c(1)-k2*c(1);
dcbdt = k1*c(1)-k3-k4*c(2);

%% Output results in a 2x1 matrix
%   The values of the two differential equations are output in a 2x1
%   matrix.

output = [dcadt;dcbdt];

end
